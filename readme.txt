Integration-Test
================
Build:
1.) ./gradlew clean build
2.) ggfs. Docker Repo aufräumen
   a) docker ps -a
   b) docker rm <ids>
   c) docker images
   d) docker rmi <ids>
   e) docker network ls
   f) docker network prune / rm
3.) Docker-Container bauen (für int)
   a) cd docker
   b) docker build -t adings/soccerdb postgres
   c) docker build -t adings/intsoccerdb integration-test
   d) docker build -t adings/soccerws tomcat
   e) wenn Netzwerk gelöscht wurde: docker network create soccerws-net
4.) INT-Test ausführen:
   a) alle ausführen mit Ausgabe: java -jar build/libs/integration-test-1.0.jar -o
   b) Liste der Tests anzeigen: java -jar build/libs/integration-test-1.0.jar -l



