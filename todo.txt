WORK  Match SOAP Schnittstelle bauen
       -> https://www.torsten-horn.de/techdocs/jee-jax-ws.htm#Infos
          --> sun-jaxws.xml

OPEN  Alle Services nach CRUD sortieren
OPEN  HateOS einfügen!
OPEN  Int-Tests fuer match erstellen
OPEN  Int Tests dockerisieren
OPEN  Wie kann ich das LOG-Level im Tomcat Docker erhöhen, sodass ich z.b.
      einen JPA/JPQL Fehler im LOG sehe?
OPEN  REST Service: Liste aller matches zu einem Spieltag?
OPEN  REST Service: Liste aller matches eines Teams (Heim und Gast)
OPEN  REST Service: Ergebnisse zu einem Match eintragen
OPEN  REST Service: Ergebnisse eines ganzen Spieltags eintragen
OPEN  Anbindung an OpenLiga (etc.)
OPEN  WebPage bauen
OPEN  CI
OPEN  MicroService Arch
OPEN  JNDI DB Verbindung
OPEN  Int Tests konfiguierbar machen
OPEN  LOGGING einbauen
        -> Logger fuer REST Service
        -> Logger fuer Service Schicht
OPEN  League in Match integrieren, dann TODO in MatchBuilder korrigieren


DONE  League Tabelle in INT Umgebung befuellen
DONE  Source in bitbucket ueberfuehrt.
