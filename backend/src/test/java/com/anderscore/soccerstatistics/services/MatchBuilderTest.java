package com.anderscore.soccerstatistics.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.data.Match;
import com.anderscore.soccerstatistics.interfaces.ClubService;
import com.anderscore.soccerstatistics.interfaces.MatchService;

@RunWith(MockitoJUnitRunner.class)
public class MatchBuilderTest {

    @Mock
    private ClubService clubService;

    @Mock
    private MatchService matchService;

    @InjectMocks
    private MatchBuilder builder;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private League league;

    @Before
    public void setUp() throws Exception {
        league = new League(0, "Major", "A");

    }

    @Test
    public void testExceptionThrownWhenYearIsWrongYear1999() {
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("2000 <= year <= 9999");
        
        builder.build(league, 1999);
    }

    @Test
    public void testExceptionThrownWhenClubsAreNull() {
        // GIVEN
        when(clubService.findByLeague(any())).thenReturn(null);
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Clubs should be found in given league.");

        builder.build(league, 2018);
    }

    @Test
    public void testExceptionThrownWhenClubListIsEmpty() {
        // GIVEN
        when(clubService.findByLeague(any())).thenReturn(new ArrayList<>());
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Club count of the given league should be more than 1.");

        builder.build(league, 2018);
    }

    @Test
    public void testWhenMatchesAlreadyExists() {
        // GIVEN
        List<Club> clubs = new ArrayList<>();
        clubs.add(new Club(0, "A", "A", "A", 1990));
        clubs.add(new Club(1, "B", "B", "A", 1990));
        
        List<Match> matches = new ArrayList<>();
        matches.add(new Match(0, clubs.get(0), clubs.get(1), 2018, 1, "2018-01-01", 0, 0, false));
        matches.add(new Match(0, clubs.get(1), clubs.get(0), 2018, 2, "2018-01-02", 0, 0, false));

        when(clubService.findByLeague(any())).thenReturn(clubs);
        when(matchService.getAllMatchesOfATeamByYear(any(), anyInt())).thenReturn(matches);
        
        List<Match> buildMatches = builder.build(league, 2018);
        assertNotNull(buildMatches);
        assertEquals(0, buildMatches.size());
    }

    private void assertMatches(List<Match> expected, List<Match> result) {
        assertEquals(expected.size(), result.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i).getHome().getIdentifier(),
                    result.get(i).getHome().getIdentifier());
            assertEquals(expected.get(i).getGuest().getIdentifier(),
                    result.get(i).getGuest().getIdentifier());
            assertEquals(expected.get(i).getSpieltag(), result.get(i).getSpieltag());
            assertEquals(expected.get(i).getYear(), result.get(i).getYear());
        }
    }

    @Test
    public void testWhenTwoClubs() {
        // GIVEN
        List<Club> clubs = new ArrayList<>();
        clubs.add(new Club(0, "A", "A", "A", 1990));
        clubs.add(new Club(1, "B", "B", "A", 1990));

        List<Match> expectedMatches = new ArrayList<>();
        expectedMatches.add(new Match(0, clubs.get(0), clubs.get(1), 2018, 1, null, 0, 0, false));
        expectedMatches.add(new Match(1, clubs.get(1), clubs.get(0), 2018, 2, null, 0, 0, false));

        when(clubService.findByLeague(any())).thenReturn(clubs);
        when(matchService.getAllMatchesOfATeamByYear(any(), anyInt())).thenReturn(new ArrayList<>());

        List<Match> buildMatches = builder.build(league, 2018);
        assertNotNull(buildMatches);
        assertMatches(expectedMatches, buildMatches);
    }

    @Test
    public void testWhenFourClubs() {
        // GIVEN
        List<Club> clubs = new ArrayList<>();
        clubs.add(new Club(0, "A", "A", "A", 1990));
        clubs.add(new Club(1, "B", "B", "A", 1990));
        clubs.add(new Club(2, "C", "C", "A", 1990));
        clubs.add(new Club(3, "D", "D", "A", 1990));

        List<Match> expectedMatches = new ArrayList<>();
        int idx = 0;
        expectedMatches.add(new Match(idx++, clubs.get(0), clubs.get(1), 2018, 1, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(0), clubs.get(2), 2018, 2, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(0), clubs.get(3), 2018, 3, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(1), clubs.get(2), 2018, 3, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(1), clubs.get(3), 2018, 2, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(2), clubs.get(3), 2018, 1, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(1), clubs.get(0), 2018, 4, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(2), clubs.get(0), 2018, 5, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(3), clubs.get(0), 2018, 6, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(2), clubs.get(1), 2018, 6, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(3), clubs.get(1), 2018, 5, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(3), clubs.get(2), 2018, 4, null, 0, 0, false));

        when(clubService.findByLeague(any())).thenReturn(clubs);
        when(matchService.getAllMatchesOfATeamByYear(any(), anyInt())).thenReturn(new ArrayList<>());

        List<Match> buildMatches = builder.build(league, 2018);
        assertNotNull(buildMatches);
        assertMatches(expectedMatches, buildMatches);

    }

    @Test
    public void testWhenThreeClubs() {
        // GIVEN
        List<Club> clubs = new ArrayList<>();
        clubs.add(new Club(0, "A", "A", "A", 1990));
        clubs.add(new Club(1, "B", "B", "A", 1990));
        clubs.add(new Club(2, "C", "C", "A", 1990));

        List<Match> expectedMatches = new ArrayList<>();
        int idx = 0;
        expectedMatches.add(new Match(idx++, clubs.get(0), clubs.get(1), 2018, 1, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(0), clubs.get(2), 2018, 2, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(1), clubs.get(2), 2018, 3, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(1), clubs.get(0), 2018, 4, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(2), clubs.get(0), 2018, 5, null, 0, 0, false));
        expectedMatches.add(new Match(idx++, clubs.get(2), clubs.get(1), 2018, 6, null, 0, 0, false));

        when(clubService.findByLeague(any())).thenReturn(clubs);
        when(matchService.getAllMatchesOfATeamByYear(any(), anyInt())).thenReturn(new ArrayList<>());

        List<Match> buildMatches = builder.build(league, 2018);
        assertNotNull(buildMatches);
        assertMatches(expectedMatches, buildMatches);
    }

}
