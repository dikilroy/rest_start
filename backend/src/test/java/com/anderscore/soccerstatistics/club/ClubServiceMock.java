package com.anderscore.soccerstatistics.club;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.NotFoundException;

import org.apache.commons.lang3.StringUtils;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.interfaces.ClubService;

/**
 * The internal data instances of the clubs are never returned by public API
 */
public class ClubServiceMock implements ClubService {

    // static mock data
    private static Club clubs[] = new Club[] {
            new Club(1l, "A-Name", "A-City", "A-Country", 1990),
            new Club(2l, "B-Name", "B-City", "B-Country", 1991),
            new Club(3l, "B-Name", "C-City", "C-Country", 1992) };

    /**
     * @param id
     * @return the original object, not a copy
     */
    private Optional<Club> internalGet(long id) {
        if (id < 0) {
            return Optional.empty();
        }

        Optional<Club> found = Arrays.stream(clubs).filter(c -> c.getIdentifier() == id).findFirst();

        return found;
    }

    private int internalGetIndex(long id) {
        for (int i = 0; i < clubs.length; i++) {
            if (clubs[i].getIdentifier() == id) {
                return i;
            }
        }

        return -1;
    }

    private Optional<Club> getByFunctionalKeys(String name, String country, String city) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(country) || StringUtils.isEmpty(city)) {
            return Optional.empty();
        }

        Optional<Club> found = Arrays.stream(clubs)
                .filter(c -> name.equals(c.getName()) && country.equals(c.getCountry()) && city.equals(c.getCity()))
                .findFirst();

        return found;
    }

    @Override
    public List<Club> get(String name) {
        List<Club> foundClubs = new ArrayList<>();

        if (name == null || name.length() == 0) {
            return foundClubs;
        }

        foundClubs = Arrays.stream(clubs)
                .filter(c -> c.getName().equalsIgnoreCase(name))
                .map(c -> new Club(c))
                .collect(Collectors.toList());

        return foundClubs;
    }

    @Override
    public List<Club> getAll() {
        List<Club> foundClubs = new ArrayList<>();
        foundClubs = Arrays.stream(clubs).map(c -> new Club(c)).collect(Collectors.toList());
        return foundClubs;

    }

    @Override
    public Club get(long id) {

        Optional<Club> club = internalGet(id);

        // copy original object
        if (club.isPresent()) {
            return new Club(club.get());
        }

        return null;

    }

    protected boolean validate(Club club) {
        if (club == null)
            return false;

        // @formatter:off
        if (StringUtils.isEmpty(club.getName()) ||
                StringUtils.isEmpty(club.getCity()) ||
                StringUtils.isEmpty(club.getCountry()) ||
                club.getFoundingYear() < 1850 || club.getFoundingYear() >= 10000 ||
                club.getIdentifier() < -1) {
            return false;
        }
        // @formatter:off

        return true;
    }

    /**
     * Updates a club via identifier if modifications exists. If club is not found a
     * NotFoundException is thrown.
     * 
     * @param club
     * @return a new instance of the modified club
     */
    private Club update(Club club) {
        if (club == null) {
            return null;
        }

        int idx = internalGetIndex(club.getIdentifier());

        if (idx < 0) {
            throw new NotFoundException(
                    String.format("Couldn't update club with id %s because it doesn't exists!", club.getIdentifier()));
        }

        Club origClub = clubs[idx];

        if (origClub.equals(club)) {
            // no change
            return new Club(club);
        }

        // check if modification create a functional duplicate!
        Optional<Club> functionalClub = getByFunctionalKeys(club.getName(), club.getCountry(), club.getCity());
        if (functionalClub.isPresent() && functionalClub.get().getIdentifier() != club.getIdentifier()) {
            // don't update to a functional duplicate!
            // TODO: maybe return the original object ?
            return null;
        }

        // TODO: synchronize this line
        clubs[idx] = new Club(club);

        return clubs[idx];
    }

    @Override
    public Club store(Club club) {
        if (club == null)
            return club;

        // check all mandatory fields
        if (!validate((Club) club)) {
            // TODO: report validation errors!
            return null;
        }

        Club updatedClub = null;

        if (club.getIdentifier() >= 0) {
            updatedClub = update((Club) club);
        } else {
            Optional<Club> found = getByFunctionalKeys(club.getName(), club.getCountry(), club.getCity());
            if (found.isPresent()) {
                // don't store functional duplicates!
                // maybe return the original object ?
                // internalClub.setIdentifier(found.get().getIdentifier());
                return null;
            }

            // TODO: slow implementation!
            long lastIdentifier = clubs[clubs.length - 1].getIdentifier();
            Club newclubs[] = Arrays.copyOf(clubs, clubs.length + 1);
            updatedClub = new Club(lastIdentifier + 1, (Club) club);
            newclubs[newclubs.length - 1] = updatedClub;

            // TODO: synchronize this line!
            clubs = newclubs;
        }

        return updatedClub;
    }

    @Override
    public List<Club> findByLeague(League league) {
        // TODO Auto-generated method stub
        return null;
    }

}
