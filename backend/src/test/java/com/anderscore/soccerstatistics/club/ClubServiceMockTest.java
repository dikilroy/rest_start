package com.anderscore.soccerstatistics.club;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.Test;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.interfaces.ClubService;

public class ClubServiceMockTest {

    private void notFound(List<Club> clubList) {
        assertThat(clubList, notNullValue());
        assertThat(clubList.size(), is(0));
        assertThat(clubList, empty());
    }

    private void found(List<Club> clubList, Club... clubs) {
        assertThat(clubList, notNullValue());
        assertThat(clubList.size(), is(clubs.length));

        List<Club> list = Arrays.asList(clubs);
        assertThat(clubList, is(list));
    }

    @Test
    public void testGetString() {
        ClubService cs = new ClubServiceMock();

        // null checks
        List<Club> list = cs.get(null);
        notFound(list);

        // not found
        list = cs.get("not found");
        notFound(list);

        // found one club
        Club expected = new Club(1l, "A-Name", "A-City", "A-Country", 1990);
        list = cs.get("A-Name");
        found(list, expected);

        // found all Clubs
        Club expectedList[] = new Club[] {
                new Club(2l, "B-Name", "B-City", "B-Country", 1991),
                new Club(3l, "B-Name", "C-City", "C-Country", 1992)
        };
        list = cs.get("B-Name");
        found(list, expectedList);
    }

    @Test
    public void testGetLong() {
        ClubService cs = new ClubServiceMock();

        // negative values check
        Club found = cs.get(-1);
        assertThat(found, nullValue());

        // not found
        found = cs.get(Long.MAX_VALUE);
        assertThat(found, nullValue()); // only this check, the others are already checked above

        found = cs.get(0); // is also not present!
        assertThat(found, nullValue()); // only this check, the others are already checked above

        // found #1
        found = cs.get(1l);
        assertThat(found, notNullValue());
        assertThat(found, is(new Club(1l, "A-Name", "A-City", "A-Country", 1990)));

        // found #2
        found = cs.get(2l);
        assertThat(found, notNullValue());
        assertThat(found, is(new Club(2l, "B-Name", "B-City", "B-Country", 1991)));

        // modify found object don't modify the internal object!
        found = cs.get(3l);
        found.setName("C-Name");
        Club origValue = cs.get(3l);
        assertThat(found.getName(), not(origValue.getName()));

    }

    @Test
    public void testStore() {
        ClubService cs = new ClubServiceMock();

        // TODO: check instances!

        // null check
        Club storedClub = cs.store(null);
        assertThat(storedClub, nullValue());

        /*
         * Validation Checks
         */

        // Wrong ID
        Club toStore = new Club(-2, "D-Name", "D-City", "D-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        // name (not null and not empty)
        toStore = new Club(1, null, "D-City", "D-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());
        toStore = new Club(1, "", "D-City", "D-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        // city (not null and not empty)
        toStore = new Club(1, "D-Name", null, "D-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());
        toStore = new Club(1, "", "", "D-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        // country (not null and not empty)
        toStore = new Club(1, "D-Name", "D-City", null, 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());
        toStore = new Club(1, "", "", "", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        // founding year (1850 <= foundingYear <10000)
        toStore = new Club(1, "D-Name", "D-City", "D-Country", 0);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());
        toStore = new Club(1, "D-Name", "D-City", "D-Country", -1);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        // boundary check
        toStore = new Club(1, "D-Name", "D-City", "D-Country", 1849);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());
        toStore = new Club(1, "D-Name", "D-City", "D-Country", 10000);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        /*
         * check insert
         */
        toStore = new Club(-1, "D-Name", "D-City", "D-Country", 1850);
        storedClub = cs.store(toStore);
        assertThat(storedClub, notNullValue());
        assertThat(storedClub.getIdentifier(), is(4l));
        Club clubToUpdate = cs.get(4);
        assertThat(clubToUpdate, notNullValue());
        assertThat(storedClub, is(clubToUpdate));

        // check functional duplicates
        toStore = new Club(-1, "A-Name", "A-City", "A-Country", 1990);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        toStore = new Club(-1, "A-Name", "A-City", "A-Country", 1869);
        storedClub = cs.store(toStore);
        assertThat(storedClub, nullValue());

        /*
         * check update
         */
        clubToUpdate.setFoundingYear(1994);
        storedClub = cs.store(clubToUpdate);
        assertThat(storedClub, notNullValue());
        assertThat(storedClub.getIdentifier(), is(4l));
        Club updatedClub = cs.get(4);
        assertThat(updatedClub, notNullValue());
        assertThat(updatedClub, is(clubToUpdate));

        //
        clubToUpdate.setName("A-Name");
        clubToUpdate.setCountry("A-Country");
        clubToUpdate.setCity("A-City");
        clubToUpdate.setFoundingYear(1990);
        storedClub = cs.store(clubToUpdate);
        assertThat(storedClub, nullValue());
    }

    public void testDeleteClub() {
        fail("Not yet implemented");
    }

    public void testDeleteLong() {
        fail("Not yet implemented");
    }

}
