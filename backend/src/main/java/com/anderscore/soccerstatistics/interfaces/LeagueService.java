package com.anderscore.soccerstatistics.interfaces;

import java.util.List;

import com.anderscore.soccerstatistics.data.League;

public interface LeagueService {
    List<League> getAll();

    List<League> getByCountry(String country);

    List<League> getByName(String name);

    League get(long id);

    League store(League league);
}
