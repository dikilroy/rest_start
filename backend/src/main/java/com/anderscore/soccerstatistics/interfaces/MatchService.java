package com.anderscore.soccerstatistics.interfaces;

import java.util.List;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.data.Match;

public interface MatchService {

    Match get(long id);

    List<Match> getAll();

    List<Match> getBySpieltagAndYear(int spieltag, int year);

    List<Match> getAllMatchesOfATeamByYear(Club team, int year);

    Match store(Match match);

    void createMatchesOfTheSeason(League league, int year);
}
