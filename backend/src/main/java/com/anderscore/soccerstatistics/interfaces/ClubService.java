package com.anderscore.soccerstatistics.interfaces;

import java.util.List;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;

public interface ClubService {

    List<Club> get(String name);

    List<Club> getAll();

    Club get(long id);

    Club store(Club club);

    List<Club> findByLeague(League league);
}