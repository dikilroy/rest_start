package com.anderscore.soccerstatistics.representation;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.data.Leagues;
import com.anderscore.soccerstatistics.interfaces.LeagueService;

@Path("/league")
public class LeagueServiceRest {

    @Autowired
    private LeagueService service;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getAll() {
        try {
            List<League> l = service.getAll();
            Leagues leagues = new Leagues(l);
            return Response.status(Status.OK).entity(leagues).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("name/{name}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getByName(@PathParam("name") String name) {
        try {
            List<League> list = service.getByName(name);
            Leagues l = new Leagues(list);
            return Response.status(Status.OK).entity(l).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("country/{country}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getByCountry(@PathParam("country") String country) {
        try {
            List<League> list = service.getByCountry(country);
            Leagues l = new Leagues(list);
            return Response.status(Status.OK).entity(l).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}
