package com.anderscore.soccerstatistics.representation;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.Clubs;
import com.anderscore.soccerstatistics.interfaces.ClubService;

@Path("/clubs")
public class ClubServiceRest {

    @Autowired
    private String welcomeMessage;

    @Autowired
    private ClubService clubService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAll() {
        try {
            List<Club> list = clubService.getAll();
            Clubs clubList = new Clubs(list);
            return Response.status(Status.OK).entity(clubList).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
        }
    }

    @GET
    @Path("/name/{name}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response get(@PathParam("name") final String name) {
        List<Club> list = clubService.get(name);
        Clubs clubList = new Clubs(list);
        return Response.status(Status.OK).entity(clubList).build();
    }

    @GET
    @Path("/id/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response get(@PathParam("id") long id) {
        Club club = clubService.get(id);
        if (club != null) {
            return Response.status(Status.OK).entity(club).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response create(Club club) {
        try {
            Club storedClub = clubService.store(club);
            return Response.status(Response.Status.CREATED).entity(storedClub).build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }

    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    /**
     * Implements a complete update. All the content should be send by client.
     * 
     * @param id
     * @param club
     * @return
     */
    public Response update(@PathParam("id") long id, Club club) {
        if (club != null && id >= 0) {
            club.setIdentifier(id);
        }

        try {
            Club storedClub = clubService.store(club);
            return Response.status(Status.CREATED).entity(storedClub).build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/welcome")
    public Response getWelcomeMessage() {
        return Response.status(Status.OK).entity(welcomeMessage).build();
    }
}
