package com.anderscore.soccerstatistics.representation;

import static javax.ws.rs.core.Response.Status.OK;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.anderscore.soccerstatistics.data.Match;
import com.anderscore.soccerstatistics.data.Matches;
import com.anderscore.soccerstatistics.interfaces.MatchService;

@Path("/matches")
public class MatchServiceRest {

    @Autowired
    private MatchService ms;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAll() {
        List<Match> matchList = ms.getAll();
        Matches matches = new Matches(matchList);

        return Response.status(OK).entity(matches).build();
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/id/{id}")
    public Response getById(@PathParam("id") long id) {
        Match m = ms.get(id);
        return Response.status(OK).entity(m).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateMatch(@PathParam("id") long id, Match match) {

        if (match != null && id >= 0) {
            match.setIdentifier(id);
        }

        try {
            ms.store(match);
            return Response.status(Status.CREATED).entity(match).build();

        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }
}
