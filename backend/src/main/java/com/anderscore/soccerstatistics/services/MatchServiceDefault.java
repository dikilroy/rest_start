package com.anderscore.soccerstatistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.data.Match;
import com.anderscore.soccerstatistics.interfaces.MatchService;
import com.anderscore.soccerstatistics.persistence.MatchRepository;

@Service
public class MatchServiceDefault implements MatchService {

    @Autowired
    private MatchRepository rep;

    @Autowired
    private MatchBuilder matchBuilder;

    @Override
    public Match get(long id) {
        return rep.getOne(id);
    }

    @Override
    public List<Match> getAll() {
        return rep.findAll();
    }

    @Override
    public List<Match> getBySpieltagAndYear(int spieltag, int year) {
        return rep.findByYearAndSpieltag(year, spieltag);
    }

    @Override
    public List<Match> getAllMatchesOfATeamByYear(Club team, int year) {
        return rep.findByHomeOrGuestAndYear(team, year);
    }

    @Override
    public Match store(Match match) {
        return rep.save(match);
    }

    @Override
    public void createMatchesOfTheSeason(League league, int year) {
        List<Match> matches = matchBuilder.build(league, year);

        if (matches.size() > 0) {
            rep.saveAll(matches);
        }
    }

}
