package com.anderscore.soccerstatistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.interfaces.LeagueService;
import com.anderscore.soccerstatistics.persistence.LeagueRepository;

@Service
public class LeagueServiceDefault implements LeagueService {

    @Autowired
    private LeagueRepository repo;

    @Override
    public List<League> getAll() {
        return repo.findAll();
    }

    @Override
    public List<League> getByCountry(String country) {
        return repo.findByCountry(country);
    }

    @Override
    public List<League> getByName(String name) {
        return repo.findByName(name);
    }

    @Override
    public League get(long id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    public League store(League league) {
        return repo.save(league);
    }

}
