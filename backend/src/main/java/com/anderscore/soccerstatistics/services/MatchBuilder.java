package com.anderscore.soccerstatistics.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.data.Match;
import com.anderscore.soccerstatistics.interfaces.ClubService;
import com.anderscore.soccerstatistics.interfaces.MatchService;


@Service
public class MatchBuilder {

    @Autowired
    private ClubService clubService;

    @Autowired
    private MatchService matchService;


    private Match createMatch(Club home, Club guest, int year) {
        Match m = new Match();
        m.setHome(home);
        m.setGuest(guest);
        m.setSpieltag(-1);
        m.setGoalsGuest(0);
        m.setGoalsHome(0);
        m.setYear(year);
        return m;
    }

    private int putMatchIntoSpieltag(Match match, ArrayList<ArrayList<Match>> spieltagMatches) {
        Assert.notNull(match, "match should not be null");

        if (spieltagMatches.size() == 0) {
            spieltagMatches.add(new ArrayList<>());

            spieltagMatches.get(0).add(match);
            return 1;
        }
        
        int idx = 0;
        for (; idx <spieltagMatches.size(); idx++) {
            List<Match> list = spieltagMatches.get(idx);

            if (!matchExists(match, list)) {
                list.add(match);
                return idx + 1;
            }
        }

        spieltagMatches.add(new ArrayList<>());
        spieltagMatches.get(idx).add(match);
        return idx + 1;
    }

    private List<Match> makeMatches(List<Club> clubs, int year) {
        List<Match> matches = new ArrayList<>();
        ArrayList<ArrayList<Match>> spieltagMatches = new ArrayList<>();

        // first half of the season
        for (int i = 0; i < clubs.size() - 1; i++) {
            for (int j = i + 1; j < clubs.size(); j++) {
                Match match = createMatch(clubs.get(i), clubs.get(j), year);
                int spieltag = putMatchIntoSpieltag(match, spieltagMatches);
                match.setSpieltag(spieltag);
                matches.add(match);
            }
        }
        
        // second half of the season
        List<Match> secondHalf = new ArrayList<>();
        for (Match m : matches) {
            Match match = createMatch(m.getGuest(), m.getHome(), year);
            int spieltag = putMatchIntoSpieltag(match, spieltagMatches);
            match.setSpieltag(spieltag);
            secondHalf.add(match);
        }

        matches.addAll(secondHalf);
        return matches;
    }

    private boolean matchExists(Match matchToCheck, List<Match> matches) {
        for (Match m : matches) {
            if (teamInMatch(matchToCheck.getHome(), m) || teamInMatch(matchToCheck.getGuest(), m)) {
                return true;
            }
        }
        return false;
    }

    private boolean teamInMatch(Club club, Match match) {
        return match.getHome().equals(club) || match.getGuest().equals(club);
    }

    public List<Match> build(League league, int year) {

        Assert.notNull(league, "League should be not null");
        Assert.isTrue(2000 <= year && year <= 9999, "2000 <= year <= 9999");

        List<Club> clubsOfTheLeague = clubService.findByLeague(league);
        Assert.notNull(clubsOfTheLeague, "Clubs should be found in given league.");
        Assert.isTrue(clubsOfTheLeague.size() > 1, "Club count of the given league should be more than 1.");
        
        // teuerer Umweg! Matches der betroffenen Clubs muss über jeden einzelnen Club
        // erfolgen!
        // TODO: League in Match integrieren
        List<Match> matches = new ArrayList<>();
        for (Club c : clubsOfTheLeague) {
            matches.addAll(matchService.getAllMatchesOfATeamByYear(c, year));
        }

        if (matches.size() > 0) {
            // TODO: Logger einbauen
            return new ArrayList<>();
        }

        matches = makeMatches(clubsOfTheLeague, year);
        return matches;
    }
}
