package com.anderscore.soccerstatistics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;
import com.anderscore.soccerstatistics.interfaces.ClubService;
import com.anderscore.soccerstatistics.persistence.ClubRepository;

@Service
public class ClubServiceDefault implements ClubService {

    @Autowired
    private ClubRepository rep;

    @Override
    public List<Club> get(String name) {
        return rep.findByName(name);
    }

    @Override
    public List<Club> getAll() {
        return rep.findAll();
    }

    @Override
    public Club get(long id) {
        Optional<Club> club = rep.findById(id);
        return club.orElse(null);
    }

    @Override
    public Club store(Club club) {
        return rep.save(club);
    }

    @Override
    public List<Club> findByLeague(League league) {
        return rep.findByLeague(league);
    }

}
