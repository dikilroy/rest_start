package com.anderscore.soccerstatistics.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.lang.NonNull;

@Entity
@XmlRootElement(name = "league")
@XmlAccessorType(XmlAccessType.FIELD)
@Table(name = "league", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "country" }))
public class League {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long identifier;

    @Column
    @NonNull
    private String name;

    @Column
    @NonNull
    private String country;

    public League() {
        super();
    }

    public League(League league) {
        this(league.getIdentifier(), league.getName(), league.getCountry());
    }

    public League(long identifier, String name, String country) {
        this();
        this.identifier = identifier;
        this.name = name;
        this.country = country;
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + (int) (identifier ^ (identifier >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof League)) {
            return false;
        }
        League other = (League) obj;
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (identifier != other.identifier) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "League [identifier=" + identifier + ", name=" + name + ", country=" + country + "]";
    }

}
