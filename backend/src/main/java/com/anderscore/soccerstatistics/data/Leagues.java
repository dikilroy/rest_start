package com.anderscore.soccerstatistics.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "leagues")
@XmlAccessorType(XmlAccessType.FIELD)
public class Leagues implements Serializable, Cloneable {
    private static final long serialVersionUID = -3135789801960815989L;

    @XmlElement
    private List<League> leagues;

    public Leagues() {
    }

    public Leagues(List<League> leagues) {
        super();
        this.leagues = leagues;
    }

    public void add(League l) {
        if (leagues == null) {
            leagues = new ArrayList<>();
        }
        leagues.add(l);
    }

    public void addCopy(League l) {
        if (l != null) {
            League league = new League(l);
            add(league);
        }
    }

    public void addCopy(List<League> leagues) {
        if (leagues != null && leagues.size() > 0) {
            for (League l : leagues) {
                addCopy(l);
            }
        }
    }

    @Override
    public Leagues clone() throws CloneNotSupportedException {
        Leagues leagues = new Leagues();
        leagues.addCopy(this.leagues);
        return leagues;
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

}
