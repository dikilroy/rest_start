package com.anderscore.soccerstatistics.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "clubs")
@XmlAccessorType(XmlAccessType.FIELD)
public class Clubs implements Serializable, Cloneable {

    private static final long serialVersionUID = 6057656946830890130L;

    @XmlElement
    private List<Club> clubs;

    public Clubs() {
    }

    /**
     * Copy Constructor, do no deep copy.
     * 
     * @param clubs
     */
    public Clubs(List<Club> clubs) {
        super();
        this.clubs = clubs;
    }

    /**
     * Add the given club without copy it.
     * 
     * @param club
     */
    public void add(Club club) {
        if (clubs == null) {
            clubs = new ArrayList<>();
        }

        clubs.add(club);
    }

    /**
     * Add a copy of the given object.
     * 
     * @param club
     */
    public void addCopy(Club club) {
        if (club != null) {
            Club copy = new Club(club);
            add(copy);
        }
    }

    /**
     * Deep copy all clubs of the given club list.
     * 
     * @param clubs
     */
    public void addCopy(List<Club> clubs) {
        if (clubs != null && clubs.size() > 0) {
            for (Club c : clubs) {
                addCopy(c);
            }
        }
    }

    @Override
    public Clubs clone() throws CloneNotSupportedException {
        Clubs clubs = new Clubs();
        clubs.addCopy(this.clubs);
        return clubs;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

}
