package com.anderscore.soccerstatistics.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.lang.NonNull;

@XmlRootElement(name = "match")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "match", uniqueConstraints = @UniqueConstraint(columnNames = { "home_id", "guest_id", "year",
        "spieltag" }))
public class Match implements Serializable {
    private static final long serialVersionUID = -1624221151720465821L;

    @XmlElement
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long identifier;

    @ManyToOne
    @JoinColumn(name = "home_id", nullable = false)
    private Club home;

    @ManyToOne
    @JoinColumn(name = "guest_id", nullable = false)
    private Club guest;

    @XmlElement
    @NonNull
    @Column(nullable = false)
    private int year;

    @XmlElement
    @NonNull
    @Column(nullable = false)
    private int spieltag;

    @XmlElement
    @NonNull
    @Column(name = "match_date", nullable = false)
    private String matchDate;

    @XmlElement
    @NonNull
    @Column(name = "goals_home", nullable = false)
    private int goalsHome;

    @XmlElement
    @NonNull
    @Column(name = "goals_guest", nullable = false)
    private int goalsGuest;

    @XmlElement
    @NonNull
    @Column(name = "has_finished", nullable = false)
    private boolean hasFinished;

    public Match() {
    }

    public Match(long identifier, Club home, Club guest, int year, int spieltag, String matchDate, int goalsHome,
            int goalsGuest, boolean hasFinished) {
        super();
        this.identifier = identifier;
        this.home = home;
        this.guest = guest;
        this.year = year;
        this.spieltag = spieltag;
        this.matchDate = matchDate;
        this.goalsHome = goalsHome;
        this.goalsGuest = goalsGuest;
        this.hasFinished = hasFinished;
    }

    public Match(Match match) {
        this(match.getIdentifier(), match.getHome(), match.getGuest(), match.getYear(), match.getSpieltag(),
                match.getMatchDate(), match.getGoalsHome(), match.getGoalsGuest(), match.isHasFinished());
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSpieltag() {
        return spieltag;
    }

    public void setSpieltag(int spieltag) {
        this.spieltag = spieltag;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public int getGoalsHome() {
        return goalsHome;
    }

    public void setGoalsHome(int goalsHome) {
        this.goalsHome = goalsHome;
    }

    public int getGoalsGuest() {
        return goalsGuest;
    }

    public void setGoalsGuest(int goalsGuest) {
        this.goalsGuest = goalsGuest;
    }

    public boolean isHasFinished() {
        return hasFinished;
    }

    public void setHasFinished(boolean hasFinished) {
        this.hasFinished = hasFinished;
    }

    public Club getHome() {
        return home;
    }

    public void setHome(Club home) {
        this.home = home;
    }

    public Club getGuest() {
        return guest;
    }

    public void setGuest(Club guest) {
        this.guest = guest;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + goalsGuest;
        result = prime * result + goalsHome;
        result = prime * result + ((guest == null) ? 0 : guest.hashCode());
        result = prime * result + (hasFinished ? 1231 : 1237);
        result = prime * result + ((home == null) ? 0 : home.hashCode());
        result = prime * result + (int) (identifier ^ (identifier >>> 32));
        result = prime * result + ((matchDate == null) ? 0 : matchDate.hashCode());
        result = prime * result + spieltag;
        result = prime * result + year;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Match)) {
            return false;
        }
        Match other = (Match) obj;
        if (goalsGuest != other.goalsGuest) {
            return false;
        }
        if (goalsHome != other.goalsHome) {
            return false;
        }
        if (guest == null) {
            if (other.guest != null) {
                return false;
            }
        } else if (!guest.equals(other.guest)) {
            return false;
        }
        if (hasFinished != other.hasFinished) {
            return false;
        }
        if (home == null) {
            if (other.home != null) {
                return false;
            }
        } else if (!home.equals(other.home)) {
            return false;
        }
        if (identifier != other.identifier) {
            return false;
        }
        if (matchDate == null) {
            if (other.matchDate != null) {
                return false;
            }
        } else if (!matchDate.equals(other.matchDate)) {
            return false;
        }
        if (spieltag != other.spieltag) {
            return false;
        }
        if (year != other.year) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Match [identifier=" + identifier + ", home=" + home + ", guest=" + guest + ", year=" + year
                + ", spieltag=" + spieltag + ", matchDate=" + matchDate + ", goalsHome=" + goalsHome + ", goalsGuest="
                + goalsGuest + ", hasFinished=" + hasFinished + "]";
    }

}
