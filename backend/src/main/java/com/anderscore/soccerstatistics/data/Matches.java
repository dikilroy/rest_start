package com.anderscore.soccerstatistics.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "matches")
@XmlAccessorType(XmlAccessType.FIELD)
public class Matches implements Serializable, Cloneable {
    private static final long serialVersionUID = 6761172804567138756L;

    @XmlElement
    private List<Match> matches;

    public Matches() {
        super();
        matches = new ArrayList<>();
    }

    public Matches(List<Match> matches) {
        super();
        this.matches = matches;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public void add(Match e) {
        matches.add(e);
    }

    public void addCopy(Match match) {
        Match cpy = new Match(match);
        add(cpy);
    }

    public void addCopy(List<Match> matches) {
        if (matches != null && matches.size() > 0) {
            for (Match m : matches) {
                addCopy(m);
            }
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Matches matches = new Matches();
        matches.addCopy(this.getMatches());
        return matches;
    }

}
