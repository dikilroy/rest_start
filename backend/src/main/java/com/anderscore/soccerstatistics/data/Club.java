package com.anderscore.soccerstatistics.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.lang.NonNull;

@XmlRootElement(name = "club")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "club", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "name", "city", "country" }), 
        @UniqueConstraint(columnNames = { "league_id", "country" })})
public class Club implements Serializable {
    private static final long serialVersionUID = 8287626113232298235L;

    @XmlElement
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long identifier;

    @XmlElement
    @Column
    @NonNull
    private String name;

    @XmlElement
    @Column
    @NonNull
    private String city;

    @XmlElement
    @Column
    @NonNull
    private String country;

    @XmlElement
    @NonNull
    @Column(name = "founding_year")
    private int foundingYear;

    @XmlElement
    @OneToOne
    @NonNull
    @JoinColumn(name = "league_id", nullable = false)
    private League league;

    public Club() {
    }

    /**
     * constructs a copy of club but with the identifier given by identifier
     * 
     * @param identifier
     * @param club
     */
    public Club(long identifier, Club club) {
        this(identifier, club.getName(), club.getCity(), club.getCountry(), club.getFoundingYear());
    }

    /**
     * Copies the given club to a new instance
     * 
     * @param club
     */
    public Club(Club club) {
        this(club.getIdentifier(), club.getName(), club.getCity(), club.getCountry(), club.getFoundingYear());
    }

    public Club(long identifier, String name, String city, String country, int foundingYear) {
        super();
        this.identifier = identifier;
        this.name = name;
        this.city = city;
        this.country = country;
        this.foundingYear = foundingYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public long getIdentifier() {
        return identifier;
    }

    public int getFoundingYear() {
        return foundingYear;
    }

    public void setFoundingYear(int foundingYear) {
        this.foundingYear = foundingYear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + foundingYear;
        result = prime * result + (int) (identifier ^ (identifier >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Club)) {
            return false;
        }
        Club other = (Club) obj;
        if (identifier != other.identifier) {
            return false;
        }
        if (city == null) {
            if (other.city != null) {
                return false;
            }
        } else if (!city.equals(other.city)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (foundingYear != other.foundingYear) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("Club [identifier=%s, name=%s, foundingYear=%s, city=%s, country=%s]", identifier, name,
                foundingYear, city, country);
    }
}
