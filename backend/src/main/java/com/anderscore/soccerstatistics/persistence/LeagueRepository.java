package com.anderscore.soccerstatistics.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anderscore.soccerstatistics.data.League;

public interface LeagueRepository extends JpaRepository<League, Long> {

    List<League> findByName(String name);

    List<League> findByCountry(String country);
}
