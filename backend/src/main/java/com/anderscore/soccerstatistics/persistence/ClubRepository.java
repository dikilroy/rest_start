package com.anderscore.soccerstatistics.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.League;

public interface ClubRepository extends JpaRepository<Club, Long> {

    List<Club> findByName(String name);

    List<Club> findByNameAndCityAndCountry(String name, String city, String country);

    List<Club> findByCountry(String country);

    List<Club> findByLeague(League league);
}
