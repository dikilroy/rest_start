package com.anderscore.soccerstatistics.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.anderscore.soccerstatistics.data.Club;
import com.anderscore.soccerstatistics.data.Match;

public interface MatchRepository extends JpaRepository<Match, Long> {

    /**
     * get all matches with year and spieltag
     * 
     * @param year
     * @param spieltag
     * @return
     */
    List<Match> findByYearAndSpieltag(int year, int spieltag);

    /**
     * get all matches of the year.
     * 
     * @param year
     * @return
     */
    List<Match> findByYear(int year);

    /**
     * get all macthes by home and guest team for the given year.
     * 
     * @param home
     * @param guest
     * @param year
     * @return
     */
    List<Match> findByHomeAndGuestAndYear(Club home, Club guest, int year);

    /**
     * gets the list of all matches where team is a home team in the given year
     * 
     * @param home
     * @param year
     * @return
     */
    List<Match> findByHomeAndYear(Club home, int year);

    /**
     * gets the list of all matches where team is a guest team in the given year
     * 
     * @param home
     * @param year
     * @return
     */
    List<Match> findByGuestAndYear(Club guest, int year);

    /**
     * gets the list of all home/guest matches of the team in the given year
     * 
     * @param home
     * @param year
     * @return
     */
    @Query("select m from Match m where (m.home = :home or m.guest = :home) and m.year = :year")
    List<Match> findByHomeOrGuestAndYear(@Param("home") Club home, @Param("year") int year);

}
