package com.anderscore.soccerstatistics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reports")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reports implements Serializable {
    private static final long serialVersionUID = -3206254925214869044L;

    @XmlElement
    private List<Report> reports = new ArrayList<>();

    public Reports() {
    }

    public void addReport(Report report) {
        reports.add(report);
    }

    public void addReports(Reports reports) {
        this.reports.addAll(reports.getReports());
    }

    public Collection<Report> getReports() {
        return reports;
    }
}
