package com.anderscore.soccerstatistics;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "club")
@XmlAccessorType(XmlAccessType.FIELD)
public class Club implements Serializable {
    private static final long serialVersionUID = 8287626113232298235L;

    @XmlElement
    private long identifier;

    @XmlElement
    private String name;

    @XmlElement
    private String city;

    @XmlElement
    private String country;

    @XmlElement
    private int foundingYear;

    @XmlElement
    private League league;

    public Club() {
    }

    public Club(long identifier, String name, String city, String country, int foundingYear, League league) {
        super();
        this.identifier = identifier;
        this.name = name;
        this.city = city;
        this.country = country;
        this.foundingYear = foundingYear;
        this.league = league;
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getFoundingYear() {
        return foundingYear;
    }

    public void setFoundingYear(int foundingYear) {
        this.foundingYear = foundingYear;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    @Override
    public String toString() {
        return "Club [identifier=" + identifier + ", name=" + name + ", city=" + city + ", country=" + country
                + ", foundingYear=" + foundingYear + ", league=" + league + "]";
    }


    @Override
    public boolean equals(Object obj) {
        if (!equalsIgnorePrimaryKey(obj)) {
            return false;
        }

        Club club = (Club) obj;

        if (identifier != club.identifier) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + foundingYear;
        result = prime * result + (int) (identifier ^ (identifier >>> 32));
        result = prime * result + ((league == null) ? 0 : league.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean equalsIgnorePrimaryKey(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Club)) {
            return false;
        }
        Club other = (Club) obj;
        if (city == null) {
            if (other.city != null) {
                return false;
            }
        } else if (!city.equals(other.city)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (foundingYear != other.foundingYear) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
