package com.anderscore.soccerstatistics;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMapper {
    private static final ObjectMapper mapper = new ObjectMapper();
    static {
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }

    public static <T> T fromJsonString(final String jsonString, final Class<T> type) {
        try {
            return mapper.readValue(jsonString, type);
        } catch (IOException e) {
            System.err.println(e);
            try {
                return type.newInstance();
            } catch (InstantiationException | IllegalAccessException e1) {
                System.err.println(e1);
                return null;
            }
        }
    }

    public static String toJson(final Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Writes the object as json in a file. The file will be overwritten if it
     * exists.
     * 
     * @param file
     * @param obj
     */
    public static void toFile(final File file, final Object obj) {
        String json = toJson(obj);

        try {
            FileUtils.write(file, json, "UTF-8");
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Write error of: " + file.getAbsolutePath() + "\nInternal error: " + e.getMessage());
        }
    }

    public static <T> T fromJsonFile(final File file, final Class<T> type) {
        String json;
        try {

            if (!file.exists()) {
                throw new IllegalArgumentException("File must exist: " + file.getAbsolutePath());
            }
            if (!file.canRead()) {
                throw new IllegalStateException("File must be readable: " + file.getAbsolutePath());
            }

            json = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Read error of: " + file.getAbsolutePath() + "\nInternal error: " + e.getMessage());
        }

        assert (json != null);

        T t = JsonMapper.fromJsonString(json, type);

        return t;
    }
}
