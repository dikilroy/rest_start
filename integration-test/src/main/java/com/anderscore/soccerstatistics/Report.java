package com.anderscore.soccerstatistics;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "report")
@XmlAccessorType(XmlAccessType.FIELD)
public class Report implements Serializable {

    private static final long serialVersionUID = -8406976081139252551L;
    @XmlElement
    private String origin;
    @XmlElement
    private Result result;
    @XmlElement
    private String reportMessage;

    public Report() {
    }

    public Report(String origin, Result result, String reportMessage) {
        this.origin = origin;
        this.result = result;
        this.reportMessage = reportMessage;
    }

    public String getReportMessage() {
        return reportMessage;
    }

    public void setReportMessage(String reportMessage) {
        this.reportMessage = reportMessage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((origin == null) ? 0 : origin.hashCode());
        result = prime * result + ((reportMessage == null) ? 0 : reportMessage.hashCode());
        result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Report)) {
            return false;
        }
        Report other = (Report) obj;
        if (origin == null) {
            if (other.origin != null) {
                return false;
            }
        } else if (!origin.equals(other.origin)) {
            return false;
        }
        if (reportMessage == null) {
            if (other.reportMessage != null) {
                return false;
            }
        } else if (!reportMessage.equals(other.reportMessage)) {
            return false;
        }
        if (result != other.result) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Report [origin=" + origin + ", result=" + result + ", reportMessage=" + reportMessage + "]";
    }

}
