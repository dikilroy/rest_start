package com.anderscore.soccerstatistics.tests.club;

import java.io.File;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.anderscore.soccerstatistics.Club;
import com.anderscore.soccerstatistics.JsonMapper;
import com.anderscore.soccerstatistics.Result;
import com.anderscore.soccerstatistics.SoccerIntegrationTest;
import com.anderscore.soccerstatistics.tests.AbstractRestTestClass;

@SoccerIntegrationTest
public class GetSecondClub extends AbstractRestTestClass {
    public GetSecondClub() {
        super();
        testName(this.getClass().getSimpleName());
        get();
        path("clubs/id/2");
        acceptJson();
    }

    @Override
    public boolean validate(Response response) {
        Club expected;

        try {
            expected = JsonMapper.fromJsonFile(new File("src/main/resources/cases/" + getTestName() + ".json"),
                    Club.class);
        } catch (IllegalArgumentException | IllegalStateException e) {
            addReport(Result.FAILED, "NO exception", e.getMessage());
            return false;
        }
        if (response == null) {
            addReport(Result.FAILED, "club is not null");
            return false;
        }

        if (response.getStatus() == Status.OK.getStatusCode()) {
            Club result = response.readEntity(Club.class);

            if (expected.equals(result)) {
                addReport(Result.SUCCESS);
                return true;
            } else {
                addReport(Result.FAILED, "club is { " + expected.toString() + " }", "{ " + result.toString() + " }");
            }
        } else {
            addReport(Result.FAILED, "Response-status is " + Status.OK.getStatusCode(),
                    String.valueOf(response.getStatus()));
        }
        return false;
    }
}
