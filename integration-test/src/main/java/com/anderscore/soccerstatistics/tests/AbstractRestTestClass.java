package com.anderscore.soccerstatistics.tests;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import com.anderscore.soccerstatistics.Report;
import com.anderscore.soccerstatistics.Reports;
import com.anderscore.soccerstatistics.Result;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public abstract class AbstractRestTestClass {
    private String testName;
    private HttpMethod method;
    private String url;
    private String path;
    private Object data;
    private String mediaType;
    private Reports reports = new Reports();

    public AbstractRestTestClass() {
        super();
    }

    public void configure(String url) {
        this.url = url;
    }

    public AbstractRestTestClass(String testName) {
        this();
        this.testName = testName;
    }

    protected AbstractRestTestClass testName(String testname) {
        this.testName = testname;
        return this;
    }

    protected AbstractRestTestClass get() {
        method = HttpMethod.GET;
        return this;
    }

    protected AbstractRestTestClass post() {
        method = HttpMethod.POST;
        return this;
    }

    protected AbstractRestTestClass put() {
        method = HttpMethod.PUT;
        return this;
    }

    protected AbstractRestTestClass acceptJson() {
        mediaType = MediaType.APPLICATION_JSON;
        return this;
    }

    protected AbstractRestTestClass acceptXml() {
        mediaType = MediaType.APPLICATION_XML;
        return this;
    }

    protected AbstractRestTestClass url(String url) {
        this.url = url;
        return this;
    }

    protected AbstractRestTestClass path(String path) {
        this.path = path;
        return this;
    }

    public AbstractRestTestClass data(Object data) {
        this.data = data;
        return this;
    }

    public boolean runTest() {
        if (StringUtils.isEmpty(url)) {
            addReport(Result.FAILED, "not empty [url, path, method]", "url is empty");
            return false;
        }
        if (StringUtils.isEmpty(path)) {
            addReport(Result.FAILED, "not empty [url, path, method]", "path is empty");
            return false;
        }
        if (method == null) {
            addReport(Result.FAILED, "not empty [url, path, method]", "method is empty");
            return false;
        }

        Client client = ClientBuilder.newClient();

        WebTarget restUrl = client.target(url).register(JacksonJsonProvider.class).path(path);
        Invocation.Builder request = restUrl.request(mediaType);

        Response response = null;
        try {
            switch (method) {
            case GET:
                response = request.get(Response.class);
                break;
            case POST: {
                request.header("content-type", MediaType.APPLICATION_JSON);
                if (data != null) {
                    response = request.post(Entity.json(getData()), Response.class);
                }
                break;
            }
            default:
                response = Response.status(Status.METHOD_NOT_ALLOWED).build();
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return validate(response);
    }

    protected void addReport(Report report) {
        reports.addReport(report);
    }

    protected void addReport(Result result, String expected) {
        reports.addReport(new Report(testName, result, "EXPECTED " + expected));
    }

    protected void addReport(Result result, String expected, String was) {
        reports.addReport(new Report(testName, result, "EXPECTED " + expected + " but WAS " + was));
    }

    protected void addReport(Result result) {
        reports.addReport(new Report(testName, result, null));
    }

    public String getTestName() {
        return testName;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getPath() {
        return path;
    }

    public Object getData() {
        return data;
    }

    public String getMediaType() {
        return mediaType;
    }

    public Reports getReports() {
        return reports;
    }

    abstract public boolean validate(Response response);

}
