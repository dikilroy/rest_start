package com.anderscore.soccerstatistics.tests;

public enum HttpMethod {
    GET, POST, PUT, DELETE;
}
