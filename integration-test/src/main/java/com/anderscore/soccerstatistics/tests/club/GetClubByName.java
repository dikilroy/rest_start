package com.anderscore.soccerstatistics.tests.club;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.anderscore.soccerstatistics.Club;
import com.anderscore.soccerstatistics.Clubs;
import com.anderscore.soccerstatistics.JsonMapper;
import com.anderscore.soccerstatistics.Result;
import com.anderscore.soccerstatistics.SoccerIntegrationTest;
import com.anderscore.soccerstatistics.tests.AbstractRestTestClass;

@SoccerIntegrationTest
/**
 * Asserts club by name in the Database
 *
 */
public class GetClubByName extends AbstractRestTestClass {
    private boolean preValidation = false;
    private String json;
    private Clubs expectedClubs;

    public GetClubByName() {
        super();
        testName(this.getClass().getSimpleName());
        get();
        path("clubs/name/B-Name");

        File f = new File("src/main/resources/cases/" + getTestName() + ".json");
        try {
            json = FileUtils.readFileToString(f, "UTF-8");
        } catch (IOException e) {
            addReport(Result.FAILED,
                    "Exception thrown while reading file '" + f.getAbsolutePath() + "': " + e.getMessage());
        }

        if (StringUtils.isEmpty(json)) {
            addReport(Result.FAILED, "input data of file '" + f.getAbsolutePath() + "' is empty");
        } else {
            expectedClubs = JsonMapper.fromJsonFile(f, Clubs.class);
            data(expectedClubs);
            if (expectedClubs == null) {
                addReport(Result.FAILED,
                        "Couldn't create a Clubs object from JSON in file <" + f.getAbsolutePath() + ">.");
            } else {
                preValidation = true;
            }
        }
    }

    @Override
    public boolean validate(Response response) {
        if (!preValidation)
            return false;

        if (response == null) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.OK.getStatusCode(),
                    Status.CREATED.getReasonPhrase()), "response is null");
            return false;
        }

        if (response.getStatus() != Status.OK.getStatusCode()) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.OK.getStatusCode(),
                    Status.OK.getReasonPhrase()), Integer.toString(response.getStatus()));
            return false;
        }
        
        Clubs result = response.readEntity(Clubs.class);
        
        if(result == null) {
            addReport(Result.FAILED, "Expected Result is not null, but it is.");
            return false;
        }
        
        if(result.getClubs() == null || result.getClubs().size() !=  expectedClubs.getClubs().size()) {
            addReport(Result.FAILED, String.format("Expect result-size is = %s", expectedClubs.getClubs().size()),
                    result.getClubs() == null ? "getClubs is null" : " size of result is " + result.getClubs().size());
            return false;
        }

        boolean testResult = true;

        for (Club rc : result.getClubs()) {
            Club expected = expectedClubs.getClubs().stream().filter(c -> c.equalsIgnorePrimaryKey(rc)).findFirst()
                    .orElse(null);
            if (expected == null) {
                addReport(Result.FAILED,
                        String.format("Expectation don't contains the given club <%s>", rc.toString()));
                testResult = false;
                continue;
            } else {
                addReport(Result.SUCCESS, String.format("Club <%s> is expected", rc.toString()));
                expectedClubs.getClubs().remove(expected);
            }
        }

        return testResult;
    }
}
