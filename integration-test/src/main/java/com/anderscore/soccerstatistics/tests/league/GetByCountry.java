package com.anderscore.soccerstatistics.tests.league;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.anderscore.soccerstatistics.JsonMapper;
import com.anderscore.soccerstatistics.League;
import com.anderscore.soccerstatistics.Leagues;
import com.anderscore.soccerstatistics.Result;
import com.anderscore.soccerstatistics.SoccerIntegrationTest;
import com.anderscore.soccerstatistics.tests.AbstractRestTestClass;

@SoccerIntegrationTest
public class GetByCountry extends AbstractRestTestClass {
    private boolean preValidation = false;
    private String json;
    private Leagues expected;

    public GetByCountry() {
        super();

        testName(this.getClass().getSimpleName());
        get();
        path("league/country/A-Country");
        acceptJson();

        File f = new File("src/main/resources/cases/" + getTestName() + ".json");
        try {
            json = FileUtils.readFileToString(f, "UTF-8");
        } catch (IOException e) {
            addReport(Result.FAILED,
                    "Exception thrown while reading file '" + f.getAbsolutePath() + "': " + e.getMessage());
        }

        if (StringUtils.isEmpty(json)) {
            addReport(Result.FAILED, "input data of file '" + f.getAbsolutePath() + "' is empty");
        } else {
            expected = JsonMapper.fromJsonFile(f, Leagues.class);
            data(expected);
            if (expected == null) {
                addReport(Result.FAILED,
                        "Couldn't create a Leagues object from JSON in file <" + f.getAbsolutePath() + ">.");
            } else {
                preValidation = true;
            }
        }

    }

    @Override
    public boolean validate(Response response) {
        if (!preValidation) {
            return false;
        }

        if (response == null) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.OK.getStatusCode(),
                    Status.CREATED.getReasonPhrase()), "response is null");
            return false;
        }

        if (response.getStatus() != Status.OK.getStatusCode()) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.OK.getStatusCode(),
                    Status.OK.getReasonPhrase()), Integer.toString(response.getStatus()));
            return false;
        }

        Leagues result = response.readEntity(Leagues.class);

        if (result == null) {
            addReport(Result.FAILED, "Expected Result is not null, but it is.");
            return false;
        }

        if (result.getLeagues() == null || result.getLeagues().size() != expected.getLeagues().size()) {
            addReport(Result.FAILED, String.format("Expect result-size is = %s", expected.getLeagues().size()),
                    result.getLeagues() == null ? "getLeagues is null"
                            : " size of result is " + result.getLeagues().size());
            return false;
        }

        boolean testResult = true;

        for (League rc : result.getLeagues()) {
            League expectedLeague = expected.getLeagues().stream().filter(c -> c.equalsIgnorePrimaryKey(rc)).findFirst()
                    .orElse(null);
            if (expectedLeague == null) {
                addReport(Result.FAILED,
                        String.format("Expectation don't contains the given club <%s>", rc.toString()));
                testResult = false;
                continue;
            } else {
                addReport(Result.SUCCESS, String.format("Club <%s> is expected", rc.toString()));
                expected.getLeagues().remove(expectedLeague);
            }
        }

        if (expected.getLeagues().size() > 0) {
            addReport(Result.FAILED,
                    String.format("Not all of the expected leagues exists in result. %s", expected.toString()));
            return false;
        }

        return testResult;
    }

}
