package com.anderscore.soccerstatistics.tests.club;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.anderscore.soccerstatistics.Club;
import com.anderscore.soccerstatistics.JsonMapper;
import com.anderscore.soccerstatistics.Result;
import com.anderscore.soccerstatistics.SoccerIntegrationTest;
import com.anderscore.soccerstatistics.tests.AbstractRestTestClass;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@SoccerIntegrationTest
/**
 * Assert post of new club will be successful. This test also gets the inserted
 * new club.
 */

public class CreateNewClub extends AbstractRestTestClass {

    private boolean preValidation = false;
    private String json;
    private Club expectedClub;

    public CreateNewClub() {
        super();
        testName(this.getClass().getSimpleName());
        post();
        path("clubs");

        File f = new File("src/main/resources/cases/" + getTestName() + ".json");

        try {
            json = FileUtils.readFileToString(f, "UTF-8");
        } catch (IOException e) {
            addReport(Result.FAILED,
                    "Exception thrown while reading file '" + f.getAbsolutePath() + "': " + e.getMessage());
        }

        if (StringUtils.isEmpty(json)) {
            addReport(Result.FAILED, "input data of file '" + f.getAbsolutePath() + "' is empty");
        } else {
            expectedClub = JsonMapper.fromJsonString(json, Club.class);
            expectedClub.setIdentifier(-1l);
            data(expectedClub);
            if (expectedClub == null) {
                addReport(Result.FAILED,
                        "Couldn't create a Club object from JSON in file <" + f.getAbsolutePath() + ">.");
            } else {
                preValidation = true;
            }
        }
    }

    @Override
    public boolean validate(Response response) {

        if (!preValidation)
            return false;

        if (response == null) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.CREATED.getStatusCode(),
                    Status.CREATED.getReasonPhrase()), "response is null");
            return false;
        }

        if (response.getStatus() != Status.CREATED.getStatusCode()) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.CREATED.getStatusCode(),
                    Status.CREATED.getReasonPhrase()), String.format("%s", response.getStatus()));
            return false;
        }

        Club result = response.readEntity(Club.class);

        if (expectedClub.equalsIgnorePrimaryKey(result)) {
            addReport(Result.SUCCESS, "Create new Club was successfull.");
            return true;
        } else {
            addReport(Result.FAILED, "club is { " + expectedClub.toString() + " }", "{ " + result.toString() + " }");
        }

        // Read the given object via rest
        Client client = ClientBuilder.newClient();
        Response responseOfGetClub = client.target(getUrl()).register(JacksonJsonProvider.class)
                .path(String.format("clubs/id/%s", result.getIdentifier())).request().get(Response.class);

        if (responseOfGetClub == null) {
            addReport(Result.FAILED, "Response of get(" + result.getIdentifier() + ") is null");
            return false;
        }

        if (responseOfGetClub.getStatus() != Status.OK.getStatusCode()) {
            addReport(Result.FAILED, String.format("Response-status = %s (%s)", Status.OK.getStatusCode(),
                    Status.CREATED.getReasonPhrase()), String.format("%s", response.getStatus()));
            return false;
        }

        Club clubReturnedByRestCall = response.readEntity(Club.class);
        if (!expectedClub.equalsIgnorePrimaryKey(clubReturnedByRestCall)) {
            addReport(Result.FAILED, "Returned Club of get(" + result.getIdentifier() + ") is " + result.toString(),
                    clubReturnedByRestCall.toString());
            return false;
        }

        return true;
    }
}
