package com.anderscore.soccerstatistics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

import com.anderscore.soccerstatistics.tests.AbstractRestTestClass;

public class IntegrationTest {
    private static Options options = new Options();
    private static CommandLine commandLine;
    private static List<AbstractRestTestClass> testObjects = new ArrayList<>();
    private static String hostUrl;

    private IntegrationTest() {
    }

    public static void main(String[] args) {
        initOptions();
        CommandLineParser parser = new DefaultParser();

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Unkown command: " + e.getMessage());
            printHelp();
            return;
        }

        if (commandLine.hasOption("h")) {
            printHelp();
            return;
        }

        readProperties();

        collectTestClasses();
        if (commandLine.hasOption("l")) {
            printTestClasses();
            return;
        }

        Reports reports = runTests();

        if (commandLine.hasOption("o") || !commandLine.hasOption("r")) {
            printResults(reports);
        }

        if (commandLine.hasOption("r")) {
            String fileName = commandLine.getOptionValue("r");
            if (StringUtils.isEmpty(fileName)) {
                System.out.println(
                        "Missing filename for the report. Check if parameter 'r' has the file argument given.");
                if (!commandLine.hasOption("o")) {
                    printResults(reports);
                }
                return;
            }

            JsonMapper.toFile(new File(fileName), reports);
        }
    }

    private static void printHelp() {
        HelpFormatter f = new HelpFormatter();
        f.printHelp("IntegrationsTest", options);
    }

    protected static void printResults(Reports reports) {
        System.out.println("\nResult of all tests:");
        for (Report r : reports.getReports()) {
            System.out.println("  + " + r.toString()); // Report formatter
        }
    }

    protected static void printTestClasses() {

        System.out.println("\nList of using test classes:");
        for (Object obj : testObjects) {
            System.out.println("   + " + obj.getClass().getName());
        }
    }

    protected static void initOptions() {
        // @formatter::off
        options.addOption(Option.builder("r")
                .hasArg()
                .argName("file")
                .longOpt("report")
                .optionalArg(true)
                .desc("Name of report file for the test results.")
                .build());
        /*
         * Only JSON is supported options.addOption(Option.builder("x") .longOpt("xml")
         * .hasArg(false) .desc("Report format is XML.") .build());
         * options.addOption(Option.builder("j") .longOpt("json") .hasArg(false)
         * .desc("Report format is JSON.") .build());
         */
        options.addOption(Option.builder("o")
                .longOpt("output")
                .hasArg(false)
                .desc("Prints the report to stdout.")
                .build());
        options.addOption(Option.builder("l")
                .longOpt("list")
                .hasArg(false)
                .desc("Lists all testclasses")
                .build());
        options.addOption(Option.builder("h")
                .longOpt("help")
                .hasArg(false)
                .desc("Prints the help.")
                .build());
        options.addOption(Option.builder("u")
                .longOpt("url")
                .argName("url")
                .hasArg()
                .desc("Sets the target URL of the host to be tested")
                .build());
        options.addOption(Option.builder("t")
                .longOpt("test")
                .hasArg()
                .argName("testClass")
                .desc("Package and class name of the class to be tested.")
                .build());
        // @formatter:on
    }

    protected static void collectTestClasses() {
        Reflections reflections = new Reflections("com.anderscore.soccerstatistics.tests");
        Set<Class<?>> testClasses = reflections.getTypesAnnotatedWith(SoccerIntegrationTest.class);

        for (Class<?> c : testClasses) {
            if (c.getSuperclass().equals(AbstractRestTestClass.class)) {

                if (!commandLine.hasOption("t")
                        || commandLine.hasOption("t") && commandLine.getOptionValue("t").equals(c.getName())) {
                    try {
                        AbstractRestTestClass testClass = (AbstractRestTestClass) c.newInstance();
                        testClass.configure(hostUrl);
                        testObjects.add(testClass);
                    } catch (InstantiationException | IllegalAccessException e) {
                        System.out.println("Can not create a new object of type " + c.getSimpleName() + " because of "
                                + e.getMessage());
                    }
                }
            }
        }
    }

    protected static void readProperties() {
        
        if (commandLine.hasOption("u")) {
            hostUrl = commandLine.getOptionValue("u");
        } else {
        
            InputStream is = IntegrationTest.class.getClassLoader()
                    .getResourceAsStream("connection.properties");

            if (is == null) {
                System.out.println("couldn't find 'connection.properties' in classpath file.");
                return;
            }

            Properties props = new Properties();
            try {
                props.load(is);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return;
            }

            if (props.containsKey("weburl")) {
                hostUrl = props.getProperty("weburl");
            } else {
                hostUrl = "http://localhost:8888/soccer/rest";
            }
        }
        
    }

    protected static Reports runTests() {

        Reports reports = new Reports();
        for (AbstractRestTestClass tc : testObjects) {

            boolean result = tc.runTest();
            reports.addReports(tc.getReports());

            if (commandLine.hasOption("o")) {
                System.out.println("Testcase #" + tc.getTestName() + " --> " + result);
            }
        }
        return reports;
    }
}
