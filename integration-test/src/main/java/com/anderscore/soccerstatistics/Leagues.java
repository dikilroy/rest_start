package com.anderscore.soccerstatistics;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "leagues")
@XmlAccessorType(XmlAccessType.FIELD)
public class Leagues implements Serializable {

    private static final long serialVersionUID = -1235377724752812591L;

    @XmlElement
    private List<League> leagues;

    public Leagues() {
        super();
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

}
