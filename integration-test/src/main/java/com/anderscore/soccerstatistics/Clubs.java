package com.anderscore.soccerstatistics;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "clubs")
@XmlAccessorType(XmlAccessType.FIELD)
public class Clubs implements Serializable, Cloneable {
    
    private static final long serialVersionUID = 8207771427881886805L;

    @XmlElement
    private List<Club> clubs;
    
    public Clubs() {}

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }
}
