create table LEAGUE(
	identifier SERIAL PRIMARY KEY,
   name varchar(100) not null,
   country varchar(100) not null,
   UNIQUE(name, country)
);

create table CLUB(
	identifier SERIAL PRIMARY KEY,
	name varchar(100) not null,
	city varchar(100) not null,
	country varchar(100) not null,
	founding_year integer not null check (founding_year > 0),
   league_id integer not null,
   UNIQUE(name, city, country),
   UNIQUE(league_id, name)
);

create table MATCH(
   identifier SERIAL PRIMARY KEY,
   year integer not null CHECK(year > 2000 AND year <= 9999) ,
   spieltag integer not null CHECK(spieltag >= 0 AND spieltag < 1000),
   match_date varchar(8) not null,
   home_id integer not null,
   guest_id integer not null,
   goals_home integer not null,
   goals_guest integer not null,
   has_finished boolean not null,
   UNIQUE(home_id, guest_id, year, spieltag)
);

