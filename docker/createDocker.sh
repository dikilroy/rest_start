#! /usr/bin/sh
docker build -t adings/soccerdb postgres
docker build -t adings/testsoccerdb testdb
docker build -t adings/intsoccerdb integration-test
docker build -t adings/soccerws tomcat
docker network create soccerws-net

