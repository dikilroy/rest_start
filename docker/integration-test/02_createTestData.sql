insert into LEAGUE (name, country) values
   ( 'Major', 'A-Country'),
   ( 'Minor', 'A-Country'),
   ( 'la premiere', 'B-Country'),
   ( 'la deuxième', 'B-Country');

insert into CLUB ( name, city, country, founding_year, league_id) values
   ( 'A-Name', 'A-City', 'A-Country', 1990, (select identifier from LEAGUE where name = 'Major')),
   ( 'B-Name', 'B-City', 'A-Country', 1991, (select identifier from LEAGUE where name = 'Major')),
   ( 'B-Name', 'B-City', 'B-Country', 1992, (select identifier from LEAGUE where name = 'la premiere'));


commit work;
