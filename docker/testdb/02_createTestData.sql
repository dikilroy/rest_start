insert into LEAGUE( name, country) values
   ( 'Major', 'A-Country'),
   ( 'Minor', 'A-Country');

insert into CLUB ( name, city, country, founding_year, league_id) values
   ( 'A', 'A', 'A-Country', 1990, (select identifier from LEAGUE where name = 'Major')),
   ( 'B', 'A', 'A-Country', 1991, (select identifier from LEAGUE where name = 'Major')),
   ( 'C', 'A', 'A-Country', 1992, (select identifier from LEAGUE where name = 'Major')),
   ( 'D', 'A', 'A-Country', 1992, (select identifier from LEAGUE where name = 'Major'));

insert into MATCH ( year, spieltag, match_date, home_id,
   guest_id, goals_home, goals_guest, has_finished) values
   ( 2018, 1, '20180801', (select identifier from CLUB where NAME='A'), (select identifier from CLUB where NAME='B') , 0, 0, false),
   ( 2018, 1, '20180801', (select identifier from CLUB where NAME='C'), (select identifier from CLUB where NAME='D') , 0, 0, false),
   ( 2018, 2, '20180807', (select identifier from CLUB where NAME='A'), (select identifier from CLUB where NAME='C') , 0, 0, false),
   ( 2018, 2, '20180807', (select identifier from CLUB where NAME='B'), (select identifier from CLUB where NAME='D') , 0, 0, false),
   ( 2018, 3, '20180807', (select identifier from CLUB where NAME='A'), (select identifier from CLUB where NAME='D') , 0, 0, false),
   ( 2018, 3, '20180807', (select identifier from CLUB where NAME='B'), (select identifier from CLUB where NAME='C') , 0, 0, false),
   ( 2018, 4, '20180801', (select identifier from CLUB where NAME='B'), (select identifier from CLUB where NAME='A') , 0, 0, false),
   ( 2018, 4, '20180801', (select identifier from CLUB where NAME='D'), (select identifier from CLUB where NAME='C') , 0, 0, false),
   ( 2018, 5, '20180807', (select identifier from CLUB where NAME='C'), (select identifier from CLUB where NAME='A') , 0, 0, false),
   ( 2018, 5, '20180807', (select identifier from CLUB where NAME='D'), (select identifier from CLUB where NAME='B') , 0, 0, false),
   ( 2018, 6, '20180807', (select identifier from CLUB where NAME='D'), (select identifier from CLUB where NAME='A') , 0, 0, false),
   ( 2018, 6, '20180807', (select identifier from CLUB where NAME='C'), (select identifier from CLUB where NAME='B') , 0, 0, false);

commit work;
